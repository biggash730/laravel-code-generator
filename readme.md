# A Laravel Code Generation Framework

This Solution will help you to generate your model classes, routes and 
controllers and then format them to serve as your reliable server side 
scripts.

## Feature Overview

- Simple and plain user interface to work with.
- Gives you options to either generate your model classes or routes and controllers.
- Uses the default laravel database connection string.
- Works best with a mysql database, work to extend it to other databases is in progress to.
- The Genrated Codes are easy to understand and extend.
- A lot more.

# SETTINGS

### Connection Settings

```php
<?php
'In the application/config/databases.php file'

on line 45 ..... 'default' => 'mysql', 

on line 70 .....'mysql' => array(
						'driver'   => 'mysql',
						'host'     => 'localhost',
						'database' => 'database',
						'username' => 'root',
						'password' => 'password',
						'charset'  => 'utf8',
						'prefix'   => '',
				),
```

## Note

Please remember to add all the dependencies in the application/libraries
and autoload them in the start.php file.
