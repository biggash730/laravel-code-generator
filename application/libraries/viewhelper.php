<?php 

class ViewHelper
{
	public static function  get_tree($user, $navModule, $page){
		echo "<div class='accordion-group'>";
		self::get_nav(0, $user, $navModule, $page);
		echo "</div>";		
	}

	public static function print_active($navmodule,$header){
      $active_header = "";
       if($navmodule === $header)
         $active_header = " in";
      return $active_header;
    }


      //page variable is sent to this page
    public static function print_active_link($page,$header){
      $active_header = "";
       if($page === $header)
         $active_header = " active";
      return $active_header;
    }

	public static function get_nav($id, &$user, $navmodule = "", &$current_page ){

		$list = Navigation::where("parent", "=", $id)->order_by("position")->get();
		//print_r($list);
		if(count($list) !== 0){
			//has children
			
			foreach ($list as $nav) {
				if($user->get_permission_for(2,$nav->id)["can_view"]){
					$collapse = $nav->name."collapse";
					if($id === 0){
						echo "<div class='accordion-heading '>";
					    echo "<a class='accordion-toggle' data-toggle='collapse' data-parent='#sidebarAccordion' href='#". $collapse."'>";
					         echo "<i class='icon ax_icon ". $nav->icon ."'></i> <span class='text'>{$nav->display_name}</span>";
					        echo "</a>";
					    echo "</div>";
					}else {
						echo "<li>";
        					echo "<a href='". URL::to_route($nav->route) ."/". $nav->id ."' class='". self::print_active_link($current_page, $nav->name ). "'>";
        						echo "<span class='icon ax_icon_small ". $nav->icon ."'></span>";
        						echo $nav->display_name;;
        					echo "</a>";
        				echo "</li>";
					}
						echo '<div id="'. $collapse .'" class="accordion-body collapse '.  self::print_active($navmodule, $nav->name ) . '">';
        					echo "<div class='accordion-inner'>";
			        			echo "<ul>";
									self::get_nav($nav->id, $user, $nav->name,$current_page);
							echo " </ul>";
				    	echo "</div>";
				    echo "</div>";
				}else {
					//left this here for debugging purposes
				}
			}
		}
	}

	public static function get_views($nav_id){
		$list = self::get_views_as_objects($nav_id);
		$out = [];
		foreach ($list as $view) {
			array_push($out, $view->name);
		}

		return $out;
	}

	public static function get_views_as_objects($nav_id){
		//todo add permission so that user cannot see this unless he has access to it
		return Navigation::find($nav_id)->views()->get();
		//return NavigationView::where("navigation_id","=",$nav_id)->get();
	}

	public static function is_view_in_array($name,$arr){
		$out = false;
		foreach ($arr as $element) {
			if($element->name === $name){
				$out = true;
				break;
			}
		}

		return $out;
	}
}