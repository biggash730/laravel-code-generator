<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Laravel Code Generator</title>
	<meta name="viewport" content="width=device-width">
	{{ HTML::style('laravel/css/style.css') }}
</head>
<body>
	<div class="wrapper">
		<header>
			<h1>Laravel Code Generator</h1>
			<h2>Trying to generate some codes</h2>

			<p class="intro-text" style="margin-top: 45px;">
			</p>
		</header>
		<div role="main" class="main">
			<div class="home">
				<h2>Generate Some Codes</h2>
				<ul>
					<li>{{ HTML::link('gen_models', 'Generate Models') }}</li>
					<li>{{ HTML::link('gen_routes_and_controllers', 'Generate the Routes and Controllers') }}</li>					
				</ul>
			</div>
		</div>
	</div>
</body>
</html>
