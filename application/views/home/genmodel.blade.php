<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Laravel: A Framework For Web Artisans</title>
	<meta name="viewport" content="width=device-width">
	{{ HTML::style('laravel/css/style.css') }}
</head>
<body>
	<div class="wrapper">
		<header>
			<h1>Laravel Code Generator</h1>
			<h2>Trying to generate some codes</h2>

			<p class="intro-text" style="margin-top: 45px;">
			</p>
		</header>
		<div role="main" class="main">
			<h2><font color="black">{{ HTML::link('/', 'Home') }}</font></h2>
			<div class="home">
				<h3>Specify the Models Directory path below</h3>
				</br>

				{{ Form::open('gen_models', 'POST') }}
				<table>					
					<tr>
						<td>{{ Form::label('dirPath', 'Models Directory Path : ') }}</td>
						<td>{{ Form::text('dirPath') }}</td>
					</tr>
					<tr>
						<td colspan=2><font color="red">{{ $errors->first('Directory Path', '<p class="error">:message</p>') }}</font></td>
					</tr>
					<tr>
						<td></td>
						<td>{{ Form::submit('Generate Models') }}</td>
					</tr>					
				</table>
				
				{{ $return_data }}	
    						
				{{ Form::close() }}

		</div>
	</div>
</div>
</body>
</html>
