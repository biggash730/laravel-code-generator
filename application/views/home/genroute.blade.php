<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Laravel Code Generator</title>
	<meta name="viewport" content="width=device-width">
	{{ HTML::style('laravel/css/style.css') }}
</head>
<body>
	<div class="wrapper">
		<header>
			<h1>Laravel Code Generator</h1>
			<h2>Trying to generate some codes</h2>

			<p class="intro-text" style="margin-top: 45px;">
			</p>
		</header>
		<div role="main" class="main">
			<h2><font color="black">{{ HTML::link('/', 'Home') }}</font></h2>
			<div class="home">
				<h3>Specify the name of the route and the model class</h3>
				</br>
				{{ Form::open('gen_routes_and_controllers', 'POST') }}
				<table>					
					<tr>
						<td>{{ Form::label('routeName', 'Route Name : ') }}</td>
						<td>{{ Form::text('routeName') }}</td>
					</tr>
					<tr>
						<td colspan=2><font color="red">{{ $errors->first('Route Name', '<p class="error">:message</p>') }}</font></td>
					</tr>
					<tr>
						<td>{{ Form::label('contName', 'Controller Name : ') }}</td>
						<td>{{ Form::text('contName') }}</td>
					</tr>
					<tr>
						<td colspan=2><font color="red">{{ $errors->first('Controller Name', '<p class="error">:message</p>') }}</font></td>
					</tr>
					<tr>
						<td>{{ Form::label('tableName', 'Table Name : ') }}</td>
						<td>{{ Form::text('tableName') }}</td>
					</tr>
					<tr>
						<td colspan=2><font color="red">{{ $errors->first('Table Name', '<p class="error">:message</p>') }}</font></td>
					</tr>
					<tr>
						<td></td>
						<td>{{ Form::submit('Generate') }}</td>
					</tr>					
				</table>
				
				{{ $return_data }}	
    						
				{{ Form::close() }}
		</div>
	</div>
</div>
</body>
</html>
