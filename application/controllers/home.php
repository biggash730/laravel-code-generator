<?php

class Home_Controller extends Base_Controller {

	public $restful = true;

	/*
	|--------------------------------------------------------------------------
	| The Default Controller
	|--------------------------------------------------------------------------
	|
	| Instead of using RESTful routes and anonymous functions, you might wish
	| to use controllers to organize your application API. You'll love them.
	|
	| This controller responds to URIs beginning with "home", and it also
	| serves as the default controller for the application, meaning it
	| handles requests to the root of the application.
	|
	| You can respond to GET requests to "/home/profile" like so:
	|
	|		public function action_profile()
	|		{
	|			return "This is your profile!";
	|		}
	|
	| Any extra segments are passed to the method as parameters:
	|
	|		public function action_profile($id)
	|		{
	|			return "This is the profile for user {$id}.";
	|		}
	|
	*/

	public function get_home()
	{
		return View::make('home.index');
	}

	public function get_models(){
		$data = '';
		return View::make('home.genmodel')
					->with('return_data', $data);

	}

	public function get_routes(){
		
		$data = '';
		return View::make('home.genroute')
					->with('return_data', $data);
	}

	public function post_models(){

		$g_model = array('Directory Path' => Input::get('dirPath'));
		$rules = array('Directory Path' => 'required|min:2|max:128');

		$v = Validator::make($g_model, $rules);
		if ( $v->fails() )
		{		
			return Redirect::to('gen_models')
						->with_errors($v);
		}
		else
		{
			$result = MyModel::generate_models(Input::all());
			return View::make('home.genmodel')
						->with('return_data',$result);
		}
	}

	public function post_routes(){
	
		$g_model = array('Route Name' => Input::get('routeName'), 'Table Name' => Input::get('tableName'), 'Controller Name' => Input::get('contName'));
		$rules = array('Route Name' => 'required|max:128', 'Table Name' => 'required|max:128', 'Controller Name' => 'required|max:128');

		$v = Validator::make($g_model, $rules);
		if ( $v->fails() )
		{		
			return Redirect::to('gen_routes_and_controllers')
						->with_errors($v);
		}
		else
		{
			$result = MyRoute::generate_routes(Input::all());
			return View::make('home.genroute')
						->with('return_data',$result);
		}

	}

}