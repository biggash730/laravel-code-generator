<?php

class MyModel extends Eloquent{

	private static $modeldir = 'application/outputs/generatedmodels';

	private static $outputdir = 'application/outputs';

	public static function generate_models($client_data){
		try{
			$models_directory_path = $client_data['dirPath'];
			
			$connection = Config::get('database.default');
			$db_con =  Config::get("database.connections.{$connection}");
			$db_name = $db_con['database'];

			if(!is_dir(MyModel::$outputdir))
				mkdir(MyModel::$outputdir);

			MyModel::remove_dir(MyModel::$modeldir);
			mkdir(MyModel::$modeldir);
			
			//return print_r($db_con);
			$db_tables = DB::query("SHOW TABLES FROM $db_name");
			
			$tb = 'tables_in_'.$db_name;

			foreach ($db_tables as $table) {
				$current_table_name = $table->$tb;
				$col_props = [];

				$result = DB::query("SHOW COLUMNS FROM ". $current_table_name);

				foreach ($result as $tb_column) {
					$props = [];

					$props['col_name'] = $tb_column->field;
					$props['col_type'] = $tb_column->type;
					
					array_push($col_props, $props);
				}

				MyModel::create_models($col_props, $current_table_name);
				
			}
			return"Models Generated Successfully";
		}catch(Exception $e){
			return print_r(HelperFunction::catch_error($e,true));
		}		
	}

	public static function create_models($column_properties, $table_name){
		
		$custom_model = file_get_contents('application/models/custommodel.php', true);

		$values = MyModel::create_values($column_properties, $table_name);

		$custom_model = MyModel::replace_keys($custom_model,$values);
		$table_name = strtolower($values['CSN']);
		file_put_contents(MyModel::$modeldir.'/'.$table_name.'.php', $custom_model);
	}

	public static function remove_dir($dirname){ 
        
		if (is_dir($dirname))
        	$dir_handle = opendir($dirname);
        else
        	return false;

        if (!$dir_handle)
        	return false;

        
        while($file = readdir($dir_handle)) {
            if ($file != "." && $file != "..") {
                if (!is_dir($dirname."/".$file))
                unlink($dirname."/".$file);
                else
                {
                    $a = $dirname.'/'.$file;
                    MyModel::remove_dir($a);
                }
            }
        }
        closedir($dir_handle);
        rmdir($dirname);
        return true;
    }

    public static function replace_keys($string, $values){ 
        
        $patterns = array('/#TBN#/','/#CSN#/','/#Csn#/','/#FNNMS#/','/#FNNMP#/','/#CRVALINP#/','/#CRVALRUL#/','/#CRINPARR#/',
        				'/#UPVALINP#/','/#UPVALRUL#/','/#UPINPARR#/','/#FLTARR#/','/#FLTQRS#/','/#QRRSTARR#/','/#RTDTARR#/');

		$replacements = array(14=>$values['TBN'],13=>$values['CSN'],12=>$values['Csn'],11=>$values['FNNMS'],10=>$values['FNNMP'],9=>$values['CRVALINP'],8=>$values['CRVALRUL'],7=>$values['CRINPARR'],
        				6=>$values['UPVALINP'],5=>$values['UPVALRUL'],4=>$values['UPINPARR'],3=>$values['FLTARR'],2=>$values['FLTQRS'],1=>$values['QRRSTARR'],0=>$values['RTDTARR']);
 
        return preg_replace($patterns, $replacements, $string);
    }

    public static function create_values($column_properties, $table_name){
		$values = [];

		$tb_name = str_replace('_', ' ', $table_name);
		$tb_name = ucwords(strtolower($tb_name));
		$values['Csn'] = HelperFunction::singularize_word($tb_name);
		$camel_table_name = str_replace(' ', '', $tb_name);

		$values['TBN'] = $table_name;
		$values['CSN'] = $camel_table_name;
		$values['FNNMP'] = $table_name;
		$values['FNNMS'] = HelperFunction::singularize_word($table_name);

		$cr_val_inp = '$inputs = array(';
		$up_val_inp = '$inputs = array(';
		$cr_val_rul = '$rules = array(';
		$up_val_rul = '$rules = array(';
		$cr_inp_arr = '$arr = DataHelper::create_audit_entries(HelperFunction::get_user_id());'. PHP_EOL;
		$up_inp_arr = '$arr = DataHelper::update_audit_entries(HelperFunction::get_user_id());'. PHP_EOL;
		$flt_arr = '$filter_array = array();'. PHP_EOL;
		$flt_qry = '';
		$qry_rst_arr = 'array(';
		$rt_dt_arr = '';

		foreach ($column_properties as $column) {

			$col_name = $column['col_name'];
			$client_col_name = $col_name;
			$client_col_name = str_replace('_', ' ', $client_col_name);
			$client_col_name = ucwords(strtolower($client_col_name));
			$client_col_name = str_replace(' ', '', $client_col_name);
			$client_col_name[0] = strtolower($client_col_name[0]);
			$col_type = $column['col_type'];

			if ($col_name != "id" && $col_name != "created_by" && $col_name != "created_at" && $col_name != "updated_by" && $col_name != "updated_at"){
				
				$cr_val_inp = $cr_val_inp.'"'.$col_name.'" => $client_data->'.$client_col_name.', '. PHP_EOL;

				if(substr($col_type, 0, 3) == 'int' || substr($col_type, 0, 7) == 'tinyint'){
					$cr_val_rul = $cr_val_rul.'"'.$col_name.'" => "required|numeric", '. PHP_EOL;
				}
				elseif(strtolower(substr($col_type, 0, 7)) == 'varchar' ){
					$cr_val_rul = $cr_val_rul.'"'.$col_name.'" => "required|max:128", '. PHP_EOL;
				}
				elseif(strtolower(substr($col_type, 0, 8)) == 'datetime' ){
					$cr_val_rul = $cr_val_rul.'"'.$col_name.'" => "required", '. PHP_EOL;
				}
				else{
					$cr_val_rul = $cr_val_rul.'"'.$col_name.'" => "required", '. PHP_EOL;
				}

				$cr_inp_arr = $cr_inp_arr.'$arr["'.$col_name.'"] = $client_data->'.$client_col_name.';'. PHP_EOL;	
			}

			if($col_name != "created_by" && $col_name != "created_at" && $col_name != "updated_by" && $col_name != "updated_at"){
				
				$up_val_inp = $up_val_inp.'"'.$col_name.'" => $client_data->'.$client_col_name.', '. PHP_EOL;

				if(substr($col_type, 0, 3) == 'int' || substr($col_type, 0, 7) == 'tinyint'){
					$up_val_rul = $up_val_rul.'"'.$col_name.'" => "required|numeric", '. PHP_EOL;
					$flt_arr = $flt_arr.'if(array_key_exists("'.$client_col_name.'", $client_data))'. PHP_EOL . '$filter_array["'.$table_name.'.'.$col_name.'"] = $client_data["'.$client_col_name.'"];'. PHP_EOL;
					$flt_qry = $flt_qry.'$query = DataHelper::filter_data($query,"'.$table_name.'.'.$col_name.'",$filter_array,"int");'. PHP_EOL;

				}
				elseif(strtolower(substr($col_type, 0, 7)) == 'varchar' || strtolower(substr($col_type, 0, 4)) == 'text'){
					$up_val_rul = $up_val_rul.'"'.$col_name.'" => "required|max:128", '. PHP_EOL;
					$flt_arr = $flt_arr.'if(array_key_exists("'.$client_col_name.'", $client_data))'. PHP_EOL . '$filter_array["'.$table_name.'.'.$col_name.'"] = "%".$client_data["'.$client_col_name.'"]."%";'. PHP_EOL;
					$flt_qry = $flt_qry.'$query = DataHelper::filter_data($query,"'.$table_name.'.'.$col_name.'",$filter_array,"string","like");'. PHP_EOL;
				}
				elseif(strtolower(substr($col_type, 0, 8)) == 'datetime' ){
					$up_val_rul = $up_val_rul.'"'.$col_name.'" => "required", '. PHP_EOL;
					$flt_arr = $flt_arr.'if(array_key_exists("'.$client_col_name.'", $client_data))'. PHP_EOL . '$filter_array["'.$table_name.'.'.$col_name.'"] = $client_data["'.$client_col_name.'"];'. PHP_EOL;
					$flt_qry = $flt_qry.'$query = DataHelper::filter_data($query,"'.$table_name.'.'.$col_name.'",$filter_array,"date");'. PHP_EOL;
				}
				else{
					$up_val_rul = $up_val_rul.'"'.$col_name.'" => "required", '. PHP_EOL;
					$flt_arr = $flt_arr.'if(array_key_exists("'.$client_col_name.'", $client_data))'. PHP_EOL . '$filter_array["'.$table_name.'.'.$col_name.'"] = $client_data["'.$client_col_name.'"];'. PHP_EOL;
					$flt_qry = $flt_qry.'$query = DataHelper::filter_data($query,"'.$table_name.'.'.$col_name.'",$filter_array,"int",);'. PHP_EOL;
				}

				$up_inp_arr = $up_inp_arr.'$arr["'.$col_name.'"] = $client_data->'.$client_col_name.';'. PHP_EOL;				
			}

			$qry_rst_arr = $qry_rst_arr.'"'.$table_name.'.'.$col_name.'",'. PHP_EOL;

			if(substr($col_type, 0, 5) == 'float'){
				$rt_dt_arr = $rt_dt_arr.'$arr["'.$client_col_name.'"] = HelperFunction::format_to_2_decimal_places($data->'.$col_name.');'. PHP_EOL;
			}
			elseif(strtolower(substr($col_type, 0, 8)) == 'datetime' ){
				$rt_dt_arr = $rt_dt_arr.'$arr["'.$client_col_name.'"] = HelperFunction::format_date_to_client($data->'.$col_name.');'. PHP_EOL;
			}
			else{
				$rt_dt_arr = $rt_dt_arr.'$arr["'.$client_col_name.'"] = $data->'.$col_name.';'. PHP_EOL;
			}
		}

		$cr_val_inp = $cr_val_inp.');';
		$up_val_inp = $up_val_inp.');';
		$cr_val_rul = $cr_val_rul.');';
		$up_val_rul = $up_val_rul.');';
		$qry_rst_arr = $qry_rst_arr.');';
		
		$values['CRVALINP'] = $cr_val_inp;
		$values['UPVALINP'] = $up_val_inp;
		$values['CRVALRUL'] = $cr_val_rul;
		$values['UPVALRUL'] = $up_val_rul;
		$values['CRINPARR'] = $cr_inp_arr;
		$values['UPINPARR'] = $up_inp_arr;
		$values['FLTARR'] = $flt_arr;
		$values['FLTQRS'] = $flt_qry;
		$values['QRRSTARR'] = $qry_rst_arr;
		$values['RTDTARR'] = $rt_dt_arr;

		return $values;
	}

	
}