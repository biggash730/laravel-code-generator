<?php
class #CSN# extends Eloquent{

	public function create_#FNNMS#($client_data){
		try{

			#CRVALINP#
			#CRVALRUL#

			$validation = MyValidator::validate_user_input($inputs,$rules);
			if($validation->fails())
				return HelperFunction::catch_error(null,false,HelperFunction::format_message($validation->errors->all()));

			#CRINPARR#
			$inserted_record = DataHelper::insert_record('#TBN#',$arr,'#Csn#');
			if(!$inserted_record['success'])
				throw new Exception($inserted_record['message']);

			return $inserted_record;
		}catch(Exception $e){
			return HelperFunction::catch_error($e,true,HelperFunction::get_admin_error_msg());
		}
	}

	public function update_#FNNMS#($client_data){
		try{

			#UPVALINP#
			#UPVALRUL#

			$validation = MyValidator::validate_user_input($inputs,$rules);
			if($validation->fails())
				return HelperFunction::catch_error(null,false,HelperFunction::format_message($validation->errors->all()));

			#UPINPARR#
			$updated_record = DataHelper::update_record('#TBN#',$arr['id'],$arr,'#Csn#');
			if(!$updated_record['success'])
				throw new Exception($updated_record['message'],1);

			return $updated_record;
		}catch(Exception $e){
			return HelperFunction::catch_error($e,true,HelperFunction::get_admin_error_msg());
		}
	}

	public function delete_#FNNMS#($id){		
		try{

			$deleted_entry = DB::table('#TBN#')->delete($id);
			return  DataHelper::return_json_data($deleted_entry,true,'#Csn# has been deleted',1);

		}catch(Exception $e){
			return HelperFunction::catch_error($e,true);
		}
	}

	public function get_#FNNMP#($client_data){
		try{
			#FLTARR#
			$query_result = DB::table('#TBN#')
								->where(function($query) use ($filter_array){				
									#FLTQRS#
							});

			$total = $query_result->count();

			$query_result = $query_result->order_by('#TBN#.id','desc');

			$paging_result = array_key_exists('page', $client_data) ? 
							HelperFunction::paginate($client_data['page'],$client_data['limit'], $query_result):$query_result;

        	//execute query and specify columns to retrive
			$result = $paging_result->get(
						#QRRSTARR#
						);

			$out = array_map(function($data){
				
					$arr = array();
					#RTDTARR#
				return $arr;
			},$result);

			return HelperFunction::return_json_data($out,true,'record loaded',$total);
		}catch(Exception $e){
			return HelperFunction::catch_error($e,true,HelperFunction::get_admin_error_msg());
		}
	}
}
