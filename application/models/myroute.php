<?php

class MyRoute extends Eloquent{

	private static $routedir = 'application/outputs/generatedroutesandconts';

	private static $outputdir = 'application/outputs';

	public static function generate_routes($client_data){
		try{
			$route_name = strtolower($client_data['routeName']);
			$cont_name = strtolower($client_data['contName']);
			$table_name = strtolower($client_data['tableName']);
			$old_routes = "<?Php";
			
			if(!is_dir(MyRoute::$outputdir))
				mkdir(MyRoute::$outputdir);

			if(!is_dir(MyRoute::$routedir))
				mkdir(MyRoute::$routedir);

			$custom_routes = file_get_contents('application/models/customroutesandcontrollers.php', true);

			//Build the values here
			$tb_name = str_replace('_', ' ', $table_name);
			$tb_name = ucwords(strtolower($tb_name));
			$md_cl_name = str_replace(' ', '', $tb_name);
			
			$values['TBNMs'] = HelperFunction::singularize_word($table_name);
			$values['RTNMs'] = HelperFunction::singularize_word($route_name);		

			$patterns = array('/#TBNM#/','/#TBNMs#/','/#RTNM#/','/#RTNMs#/','/#CTNM#/','/#MDCLNM#/');

			$replacements = array(5=>$table_name,4=>$values['TBNMs'],3=>$route_name,
									2=>$values['RTNMs'],1=>$cont_name,0=>$md_cl_name);
 
        	$custom_routes = preg_replace($patterns, $replacements, $custom_routes);

			if (file_exists(MyRoute::$routedir.'/generatedroutes.php')){

				$old_routes = file_get_contents(MyRoute::$routedir.'/generatedroutes.php', true);
			}

			$old_routes = $old_routes.PHP_EOL;
			$custom_routes = $old_routes.$custom_routes;

			file_put_contents(MyRoute::$routedir.'/generatedroutes.php', $custom_routes);
			
			return"Routes and Controllers Generated Successfully";
		}catch(Exception $e){
			return print_r(HelperFunction::catch_error($e,true));
		}		
	}

	
}