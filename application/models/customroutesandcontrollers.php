/////////////////////////////#TBNM#////////////////////////

//#RTNM#
Route::post('#RTNM#',array('uses' => '#CTNM#@#RTNMs#'));
Route::put("#RTNM#/(:num)",array('uses' => '#CTNM#@#RTNMs#'));
Route::get('#RTNM#',array('uses' => '#CTNM#@#RTNM#'));
Route::delete('#RTNM#/(:num)',array('uses' => '#CTNM#@#RTNMs#'));

//Controllers for #RTNM#
	public function post_#RTNMs#(){
		return Response::json(#MDCLNM#::create_#TBNMs#(Input::json()));
	}
	public function put_#RTNMs#(){		
		return Response::json(#MDCLNM#::update_#TBNMs#(Input::json()));
	}
	public function delete_#RTNMs#($num){
		return Response::json(#MDCLNM#::delete_#TBNMs#($num));
	}
	public function get_#RTNM#(){
		return Response::json(#MDCLNM#::get_#TBNM#(Input::all()));
	}

