<?php

class Create_Roles {

	/**
	 *this function will create the roles table
	 * and the specified columns.
	 * @return void
	 */
	public function up()
	{
		// create the roles table
		Schema::create('roles', function($table) {	
			$table->engine = 'InnoDB';		
		    $table->increments('id');
		    $table->string('name', 128)->unique();
		    $table->string('description', 128)->nullable();	
		    $table->integer('created_by')->unsigned();//->foreign()->references('id')->on('users');
		    $table->integer('updated_by')->unsigned();//->foreign()->references('id')->on('users');
			$table->timestamps();	
		});

		// insert default roles
		DB::table('roles')->insert(array(
			'name' => 'Administrator',
            'description'	=> 'Administrator role',
			'created_by' => 1,
			'updated_by' => 1,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
		));

		DB::table('roles')->insert(array(
			'name' => 'Appointments',
            'description'	=> 'These users can be assigned Appointments',
			'created_by' => 1,
			'updated_by' => 1,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
		));
	}

	/**
	 * this function will drop the roles table.
	 *
	 * @return void
	 */
	public function down()
	{
		//Schema::drop('roles');
	}

}