<?php

class Create_Users {

	/**
	 *this function will create the users table
	 * and the specified columns.
	 * @return void
	 */
	public function up()
	{
		// create the users table
		Schema::create('users', function($table) {
			$table->engine = 'InnoDB';
		    $table->increments('id');
		    $table->string('first_name', 128);
		    $table->string('last_name', 128);
		    $table->string('user_name', 128);
		    $table->string('other_names', 128)->nullable();
		    $table->string('email', 128);
		    $table->string('phone', 45)->nullable();
		    $table->string('password', 128);	    
		    $table->string('picture_file_name', 128)->nullable();
		    $table->integer('role_id')->unsigned();
		    $table->integer('created_by')->unsigned();
		    $table->integer('updated_by')->unsigned();
		    $table->timestamps();		    
		});	

		// insert default users
		DB::table('users')->insert(array(
			'first_name' => 'Ernest',
			'last_name' => 'Kobi',
			'user_name' => 'Admin',
			'other_names' => 'Kojo',
            'email'	=> 'kojo@pharmacy.com',
            'phone' => '0244556699',
            'password'	=> Hash::make('admin'),
            'picture_file_name'	=> '',
            'role_id'	=> 1,
			'created_by' => 1,
			'updated_by' => 1,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
		));

		DB::table('users')->insert(array(
			'first_name' => 'Selasie',
			'last_name' => 'Hanson',
			'user_name' => 'selasie',
			'other_names' => '',
            'email'	=> 'selasie@qf.com',
            'phone' => '0244556699',
            'password'	=> Hash::make('selasie'),
            'picture_file_name'	=> '',
            'role_id'	=> 2,
			'created_by' => 1,
			'updated_by' => 1,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
		));

		DB::table('users')->insert(array(
			'first_name' => 'Albert',
			'last_name' => 'Adjei',
			'user_name' => 'sandra',
			'other_names' => 'Olonka',
            'email'	=> 'kojo@pharmacy.com',
            'phone' => '0244556699',
            'password'	=> Hash::make('sandra'),
            'picture_file_name'	=> '',
            'role_id'	=> 2,
			'created_by' => 1,
			'updated_by' => 1,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
		));
	}

	/**
	 * this function will drop the database tables.
	 *
	 * @return void
	 */
	public function down()
	{
		//	
	}

}